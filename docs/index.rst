#############
How-To Sphinx
#############



.. note::

   This project is under active development.


.. toctree::
   :caption: Contents
   :maxdepth: 1

   why.rst
   relation_sphinx_needs
   how-to-write-rst
   sphinx-needs-basics
   architecture-examples
   import_needs

Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Tests
*****

.. :hello:`Woorld` :hello:`Woorld2`

.. disabled codeoption as the directive is even disabled
.. codeoption : : py
   :option2: helllo_to_you

   import hello
   hello()

